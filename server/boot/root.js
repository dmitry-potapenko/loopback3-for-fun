'use strict';

const exphbs = require('express-handlebars');
const path = require('path');

module.exports = function(server) {
  server.set('views', path.resolve(__dirname, '../views'));

  server.engine('handlebars', exphbs({
    defaultLayout: 'main',
    layoutsDir: 'server/views/layouts',
    partialsDir: 'server/views/partials',
  }));

  server.set('view engine', 'handlebars');
};
