'use strict';

module.exports = function(server) {
  server.get('/', (req, res) => {
    return res.render('index', {
      title: 'LoopBack Powered',
    });
  });
};
