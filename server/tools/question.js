'use strict';

const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

process.on('exit', _ => {
  rl.close();
});

module.exports = (questionText, rules) => {
  let regex = new RegExp(rules);

  return new Promise((resolve, reject) => {
    rl.question(`${questionText} ${rules}: `, answer => {
      resolve(regex.test(answer));
    });
  });
};
