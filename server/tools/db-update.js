'use strict';

const app = require('../server');
const models = require('../model-config.json');
const question = require('./question');

const db = app.dataSources.psqlDev;
db.setMaxListeners(0);

function finalize(status) {
  console.log(`Finish ${module.filename}: "${status}"`);
  process.exit();
}

function isActual(modelsNames = []) {
  return new Promise((resolve, reject) => {
    db.isActual(modelsNames, (err, isActual) => {
      if (err) {
        return reject(err);
      }
      resolve(!!isActual);
    })
  });
}

function update(modelsNames = []) {
  return new Promise((resolve, reject) => {
    db.autoupdate(modelsNames, err => {
      if (err) {
        return reject(err);
      }
      resolve({});
    });
  });
};

async function main() {
  let modelsNames = Object.keys(models).filter(mName => !mName.startsWith('_'));
  let res;

  try {
    if (await isActual('Order')) {
      return "Nothing todo!";
    }
    res = await question('It will update your DB models. Are you sure?', '(Y|y|[Yy]es)');
    if (!res) {
      return "Declined";
    }
    res = await update(modelsNames);

  } catch (err) {
    console.log(`Error ${module.filename}:`, err);
  }
  return 'Success';
}

if (require.main === module) {
  main()
    .then(finalize)
    .catch(finalize);
}
