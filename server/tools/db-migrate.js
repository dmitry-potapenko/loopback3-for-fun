'use strict';

const app = require('../server');
const models = require('../model-config.json');
const question = require('./question');

function finalize(status) {
  console.log(`Finish ${module.filename}: "${status}"`);
  process.exit();
}

function migrate(modelsNames = []) {
  const db = app.dataSources.psqlDev;

  return new Promise((resolve, reject) => {
    db.automigrate(modelsNames, err => {
      if (err) {
        return reject(err);
      }
      resolve({});
    });
  });
};

async function main() {
  let modelsNames = Object.keys(models).filter(mName => !mName.startsWith('_'));
  let res;

  try {
    res = await question('It will drop your DB models. Are you sure?', '(Y|y|[Yy]es)');
    if (!res) {
      return "Declined";
    }
    res = await migrate(modelsNames);
  } catch (err) {
    console.log(`Error ${module.filename}:`, err);
    return "Error";
  }
  return "Success";
}

if (require.main === module) {
  main()
    .then(finalize)
    .catch(finalize);
}
